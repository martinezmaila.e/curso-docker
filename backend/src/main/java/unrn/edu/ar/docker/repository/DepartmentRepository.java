package unrn.edu.ar.docker.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import unrn.edu.ar.docker.model.Department;

import java.util.List;

/**
 * Created by achalise on 2/6/17.
 */

@Repository
public interface DepartmentRepository extends MongoRepository<Department, String> {

    Department findByName(String name);

    List<Department> findDepartmentsByNameLike(String name);

}
